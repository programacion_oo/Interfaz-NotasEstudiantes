package vista;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import otros.Calculo;

import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.Random;

public class Vista3 extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	DataInputStream archivo = null;
	private static Calculo calculo;
	private static Vista2 vista2=new Vista2();
	public String dat[]=new String[vista2.n]; 
	public double not[][]=new double[vista2.n][4];
	public int cam=0;

	public Vista3() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblEscrribaElNombre = new JLabel("Escrriba el nombre que deasea modificar");
		lblEscrribaElNombre.setBounds(8, 1, 304, 15);
		contentPane.add(lblEscrribaElNombre);

		textField = new JTextField();
		textField.setBounds(98, 28, 214, 19);
		contentPane.add(textField);
		textField.setColumns(10);

		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setBounds(12, 114, 70, 15);
		contentPane.add(lblNombre);

		JLabel lblParcialI = new JLabel("Parcial I");
		lblParcialI.setBounds(12, 141, 70, 15);
		contentPane.add(lblParcialI);

		JLabel lblParcialIi = new JLabel("Parcial II");
		lblParcialIi.setBounds(12, 168, 70, 15);
		contentPane.add(lblParcialIi);

		JLabel lblExamenFinal = new JLabel("Examen final");
		lblExamenFinal.setBounds(8, 195, 89, 15);
		contentPane.add(lblExamenFinal);

		textField_1 = new JTextField();
		textField_1.setEnabled(false);
		textField_1.setBounds(113, 112, 114, 19);
		contentPane.add(textField_1);
		textField_1.setColumns(10);

		textField_2 = new JTextField();
		textField_2.setEnabled(false);
		textField_2.setBounds(113, 139, 114, 19);
		contentPane.add(textField_2);
		textField_2.setColumns(10);

		textField_3 = new JTextField();
		textField_3.setEnabled(false);
		textField_3.setBounds(113, 166, 114, 19);
		contentPane.add(textField_3);
		textField_3.setColumns(10);

		textField_4 = new JTextField();
		textField_4.setEnabled(false);
		textField_4.setBounds(113, 193, 114, 19);
		contentPane.add(textField_4);
		textField_4.setColumns(10);
		JButton btnBuscar = new JButton("Buscar");
		btnBuscar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				String buscar = textField.getText();
				String nombre = "";
				double parcial1=0;
				double parcial2=0;
				double examfinal=0;
				double definitiva=0;
				int temporal = 0;
				int i=0;
				try {

					// * abriendo archivo para lectura */

					archivo = new DataInputStream(new FileInputStream("datos"));

					// leyendo archivo

					while (true) {
						nombre = archivo.readUTF();
						parcial1 = archivo.readDouble();
						parcial2 = archivo.readDouble();
						examfinal = archivo.readDouble();
						definitiva = archivo.readDouble();
						dat[i]=nombre;
						not[i][0]=parcial1;
						not[i][1]=parcial2;
						not[i][2]=examfinal;
						not[i][3]=definitiva;
						i++;
						if (buscar.equals(nombre)) {
							textField_1.setText(nombre);
							textField_2.setText(String.valueOf(parcial1));
							textField_3.setText(String.valueOf(parcial2));
							textField_4.setText(String.valueOf(examfinal));
							textField_1.setEnabled(true);
							textField_2.setEnabled(true);
							textField_3.setEnabled(true);
							textField_4.setEnabled(true);
							temporal++;
							cam=i;
						}
					}
				}

				catch (FileNotFoundException fnfe) {
					/* Archivo no encontrado */ } catch (IOException ioe) {
					/* Error al escribir */ }
				if (temporal < 1) {
					System.out.println("nose ");
				}
			}
		});
		btnBuscar.setBounds(98, 59, 117, 25);
		contentPane.add(btnBuscar);

		JButton btnGuardar = new JButton("Guardar");
		btnGuardar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				cam--;
				String buscar = textField.getText();
				String nombre = "";
				String paso;
				double parcial1;
				double parcial2;
				double examfinal;
				double definitiva;
				int temporal = 0;
				calculo = new Calculo();
				nombre = textField_1.getText();
				System.out.println(nombre);
				parcial1 = Double.parseDouble(textField_2.getText());
				parcial2 = Double.parseDouble(textField_3.getText());
				examfinal = Double.parseDouble(textField_4.getText());
				definitiva = calculo.definitiva(parcial1, parcial2, examfinal);
				dat[cam]=nombre;
				not[cam][0]=parcial1;
				not[cam][1]=parcial2;
				not[cam][2]=examfinal;
				not[cam][3]=definitiva;
				FileOutputStream fso;
				try {
					fso = new FileOutputStream("datos");
					fso.close();
				} catch ( IOException e) {
				}
				int i=0;
				DataOutputStream archivo = null;
				try {
					
					//* Creando y grabando a un archivo, esta larga la instrucci�n*/
					archivo = new DataOutputStream( new FileOutputStream("datos",true) );
					
					while( i<vista2.n) {
						archivo.writeUTF(dat[i]);
						archivo.writeDouble(not[i][0]);
						archivo.writeDouble(not[i][1]);
						archivo.writeDouble(not[i][2]);
						archivo.writeDouble(not[i][3]);
						i++;
					}
						
						
						
						//Cierra el Archivo
						archivo.close();
					}
					
					catch(FileNotFoundException fnfe) { /* Archivo no encontrado */ }
					catch (IOException ioe) { /* Error al escribir */ }
				dispose();
				vista2.setVisible(true);
			}
		});
		btnGuardar.setBounds(65, 230, 117, 25);
		contentPane.add(btnGuardar);
	}
}
