package vista;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import otros.Calculo;

import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.awt.Font;
import java.awt.Color;

public class Vista1 extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	public static String nombre;
	public static double parcial1;
	public static double parcial2;
	public static double examfinal;
	public static double definitiva;
	private static Calculo calculo;
	private static Vista2 vista2;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Vista1 frame = new Vista1();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public Vista1() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 617, 267);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNombre = new JLabel("Nombre:");
		lblNombre.setBounds(301, 10, 68, 14);
		contentPane.add(lblNombre);
		
		JLabel lblParcialI = new JLabel("Parcial I");
		lblParcialI.setBounds(10, 35, 68, 14);
		contentPane.add(lblParcialI);
		
		JLabel lblParciaIi = new JLabel("Parcial II");
		lblParciaIi.setBounds(207, 35, 68, 14);
		contentPane.add(lblParciaIi);
		
		JLabel lblExamenFinal = new JLabel("Examen final");
		lblExamenFinal.setBounds(388, 35, 101, 14);
		contentPane.add(lblExamenFinal);
		
		textField = new JTextField();
		textField.setBounds(499, 32, 93, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setBounds(285, 35, 86, 20);
		contentPane.add(textField_1);
		textField_1.setColumns(10);
		
		textField_2 = new JTextField();
		textField_2.setBounds(69, 32, 86, 20);
		contentPane.add(textField_2);
		textField_2.setColumns(10);
		
		textField_3 = new JTextField();
		textField_3.setBounds(378, 4, 214, 20);
		contentPane.add(textField_3);
		textField_3.setColumns(10);
		
		JLabel lblDefinitiva = new JLabel("Definitiva:");
		lblDefinitiva.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblDefinitiva.setForeground(Color.BLACK);
		lblDefinitiva.setBounds(10, 88, 86, 14);
		contentPane.add(lblDefinitiva);
		
		JLabel label = new JLabel("");
		label.setBounds(107, 88, 46, 14);
		contentPane.add(label);
		
		JButton btnGuardar = new JButton("Guardar");
		btnGuardar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				calculo= new Calculo();
				nombre=textField_3.getText();
				parcial1=Double.parseDouble(textField_1.getText());
				parcial2=Double.parseDouble(textField_2.getText());
				examfinal=Double.parseDouble(textField.getText());
				definitiva=calculo.definitiva(parcial1, parcial2, examfinal);
				if(definitiva <3) {
					label.setForeground(Color.RED);
					label.setText(String.valueOf(definitiva));
				}
				if(definitiva <= 4 &&definitiva >= 3) {
					label.setForeground(Color.BLUE);
					label.setText(String.valueOf(definitiva));
				}
				if(definitiva<=5 && definitiva >4) {
					label.setForeground(Color.GREEN);
					label.setText(String.valueOf(definitiva));
				}
				DataOutputStream archivo = null;
				try {
					
					//* Creando y grabando a un archivo, esta larga la instrucci�n*/
					
						archivo = new DataOutputStream( new FileOutputStream("datos",true) );
						archivo.writeUTF(nombre);
						archivo.writeDouble(parcial1);
						archivo.writeDouble(parcial2);
						archivo.writeDouble(examfinal);
						archivo.writeDouble(definitiva);
						
						//Cierra el Archivo
						archivo.close();
					}
					
					catch(FileNotFoundException fnfe) { /* Archivo no encontrado */ }
					catch (IOException ioe) { /* Error al escribir */ }
			}
		});
		btnGuardar.setBounds(10, 163, 101, 23);
		contentPane.add(btnGuardar);
		
		JButton btnBorrar = new JButton("Borrar");
		btnBorrar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				dispose();
				Vista1 mostrar=new Vista1();
				mostrar.setVisible(true);
			}
		});
		btnBorrar.setBounds(140, 163, 89, 23);
		contentPane.add(btnBorrar);
		
		JLabel lbldeseaVerTodos = new JLabel("\u00BFDesea ver todos los datos en una tabla?");
		lbldeseaVerTodos.setBounds(301, 167, 251, 14);
		contentPane.add(lbldeseaVerTodos);
		
		JButton btnSi = new JButton("Ver");
		btnSi.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				vista2=new Vista2();
				dispose();
				vista2.setVisible(true);
				
			}
		});
		btnSi.setBounds(586, 163, 61, 23);
		contentPane.add(btnSi);
		
		JLabel lblIngreseDatosA = new JLabel("Ingrese datos en las siguientes celdas: ");
		lblIngreseDatosA.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblIngreseDatosA.setBounds(10, 0, 281, 14);
		contentPane.add(lblIngreseDatosA);
	}
}
