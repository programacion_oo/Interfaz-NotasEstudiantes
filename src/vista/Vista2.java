package vista;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTable;
import javax.swing.JButton;
import javax.swing.table.DefaultTableModel;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import javax.swing.JLabel;

public class Vista2 extends JFrame {

	private JPanel contentPane;
	private JTable table;
	private static Vista3 vista3;
	private static Vista4 vista4;
	private static Vista1 vista1;
	DataInputStream archivo = null;
	public static int n=0;
	public Vista2() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 756, 367);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		table = new JTable();
		table.setModel(new DefaultTableModel(
			new Object[][] {
				{"Nombre", "ParcialI", "ParcialII", "examen final", "definitiva"},
			},
			new String[] {
				"Nombre", "ParcialI", "ParcialII", "examen final", "definitiva"
			}
		));
		table.getColumnModel().getColumn(0).setPreferredWidth(129);
		table.getColumnModel().getColumn(3).setPreferredWidth(93);
		table.setBounds(0, 82, 503, 108);
		contentPane.add(table);
		JButton btnCargar = new JButton("cargar");
		btnCargar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				n=0;
				
				try{
					
					//* abriendo archivo para lectura */
					
					archivo = new DataInputStream( new FileInputStream("datos") );
					
					//leyendo archivo
					int numcols=table.getModel().getColumnCount();
					Object [] fila=new Object[numcols];
					while (true)
					{
						fila[0]=archivo.readUTF();
						fila[1]=archivo.readDouble();
						fila[2]=archivo.readDouble();
						fila[3]=archivo.readDouble();
						fila[4]=archivo.readDouble();
						((DefaultTableModel) table.getModel()).addRow(fila);
						n++;
					} 
				   }
					
					catch(FileNotFoundException fnfe) { /* Archivo no encontrado */ }
					catch (IOException ioe) { /* Error al escribir */ }
				
			}
		});
		btnCargar.setBounds(10, 11, 89, 23);
		contentPane.add(btnCargar);
		
		JLabel lbldeseaEditarLos = new JLabel("¿Desea editar los datos?");
		lbldeseaEditarLos.setBounds(12, 202, 187, 15);
		contentPane.add(lbldeseaEditarLos);
		
		JButton btnSi = new JButton("Si");
		btnSi.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				vista3=new Vista3();
				dispose();
				vista3.setVisible(true);
			}
		});
		btnSi.setBounds(218, 197, 47, 25);
		contentPane.add(btnSi);
		
		JLabel lbldeseaBorrarUn = new JLabel("\u00BFDesea borrar un destudiante?");
		lbldeseaBorrarUn.setBounds(10, 228, 148, 14);
		contentPane.add(lbldeseaBorrarUn);
		
		JButton btnSi_1 = new JButton("si");
		btnSi_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				dispose();
				vista4=new Vista4();
				vista4.setVisible(true);
				
			}
		});
		btnSi_1.setBounds(218, 224, 47, 23);
		contentPane.add(btnSi_1);
		
		JLabel lbldeseaAgregarUn = new JLabel("\u00BFDesea agregar un estudiante?");
		lbldeseaAgregarUn.setBounds(10, 253, 158, 14);
		contentPane.add(lbldeseaAgregarUn);
		
		JButton btnSi_2 = new JButton("si");
		btnSi_2.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				dispose();
				vista1=new Vista1();
				vista1.setVisible(true);
			}
		});
		btnSi_2.setBounds(218, 249, 47, 23);
		contentPane.add(btnSi_2);
	}
}
