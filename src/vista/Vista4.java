package vista;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class Vista4 extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private static Vista2 vista2 = new Vista2();
	public String dat[] = new String[vista2.n];
	public double not[][] = new double[vista2.n][4];
	public int cam = 0;

	public Vista4() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblEstudiante = new JLabel("estudiante");
		lblEstudiante.setBounds(10, 11, 73, 14);
		contentPane.add(lblEstudiante);

		textField = new JTextField();
		textField.setBounds(97, 8, 86, 20);
		contentPane.add(textField);
		textField.setColumns(10);

		JButton btnBorrar = new JButton("Borrar");
		btnBorrar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				DataInputStream archivo = null;
				String buscar = textField.getText();
				String nombre = "";
				double parcial1 = 0;
				double parcial2 = 0;
				double examfinal = 0;
				double definitiva = 0;
				int temporal = 0;
				int i = 0;
				try {

					// * abriendo archivo para lectura */

					archivo = new DataInputStream(new FileInputStream("datos"));

					// leyendo archivo

					while (true) {
						nombre = archivo.readUTF();
						parcial1 = archivo.readDouble();
						parcial2 = archivo.readDouble();
						examfinal = archivo.readDouble();
						definitiva = archivo.readDouble();
						dat[i] = nombre;
						not[i][0] = parcial1;
						not[i][1] = parcial2;
						not[i][2] = examfinal;
						not[i][3] = definitiva;
						i++;
						if (buscar.equals(nombre)) {
							cam = i;
							temporal++;
						}
					}
				}

				catch (FileNotFoundException fnfe) {
					/* Archivo no encontrado */ } catch (IOException ioe) {
					/* Error al escribir */ }
				if (temporal < 1) {
					System.out.println("nose ");
				} else {
					FileOutputStream fso;
					try {
						fso = new FileOutputStream("datos");
						fso.close();
					} catch (IOException e) {
					}
					cam--;
					for (i = 0; i < vista2.n; i++) {
						if (i == cam) {
							dat[i] = "";
							not[i][0] = 0;
							not[i][1] = 0;
							not[i][2] = 0;
							not[i][3] = 0;
						}
					}
					dat[cam] = dat[vista2.n - 1];
					not[cam][0] = not[vista2.n - 1][0];
					not[cam][1] = not[vista2.n - 1][1];
					not[cam][2] = not[vista2.n - 1][2];
					not[cam][3] = not[vista2.n - 1][3];
					i = 0;
					DataOutputStream arch = null;
					System.out.println(vista2.n);
					if ((vista2.n-1) > 0) {
						try {

							// * Creando y grabando a un archivo, esta larga la instrucci�n*/
							arch = new DataOutputStream(new FileOutputStream("datos", true));

							while (i < (vista2.n - 1)) {
								arch.writeUTF(dat[i]);
								arch.writeDouble(not[i][0]);
								arch.writeDouble(not[i][1]);
								arch.writeDouble(not[i][2]);
								arch.writeDouble(not[i][3]);
								i++;
							}

							// Cierra el Archivo
							archivo.close();
						}

						catch (FileNotFoundException fnfe) {
							/* Archivo no encontrado */ } catch (IOException ioe) {
							/* Error al escribir */ }
					}
						dispose();
						vista2.setVisible(true);
					
				}
			}

		});
		btnBorrar.setBounds(48, 53, 89, 23);
		contentPane.add(btnBorrar);
	}
}
